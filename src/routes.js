import React from "react";
import { Route, Routes, Navigate, BrowserRouter } from 'react-router-dom';
import Create from "./pages/task-page/create";
import TaskPage from "./pages/task-page/taskpage";
import RegisterPage from "./pages/register/register";
import LoginPage from "./pages/auth/loginPage";

import {UserContext} from "./pages/auth/user-context.js"

function AppRoutes() {
  const {token} = React.useContext(UserContext);

  if (token || localStorage.getItem('token')) {
    return (
      <div className="App">
        <BrowserRouter>
        <Routes>
          <Route path="/task" element={<TaskPage/>}/>
          <Route path="/register" element={<Navigate replace to="/task"/>}/>
          <Route path="/login" element={<Navigate replace to="/task"/>}/>
        </Routes>
        </BrowserRouter>
      </div>
      );
  } else {
    return (
      <div className="App">
        <BrowserRouter>
        <Routes>
          <Route exact path="*" element={<Navigate replace to="/login"/>}/>
          <Route path="/register" element={<RegisterPage/>}/>
          <Route path="/login" element={<LoginPage/>}/>
        </Routes>
        </BrowserRouter>
      </div>
      );
  }
  
  }

export default AppRoutes;
