import logo from './logo.svg';
import './App.css';
import AppRoutes from "./routes";

import { UserProvider } from "./pages/auth/user-context.js"

function App() {
  return (
    <UserProvider>
      <AppRoutes>
      </AppRoutes>
    </UserProvider>

  )
}

export default App;
