import React, { useState, useContext } from 'react';
import { useNavigate } from "react-router-dom";
import {
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Button
} from '@chakra-ui/react';
import './loginPage.css'

import axios from "axios";
import qs from 'qs';

import { UserContext } from './user-context.js'


const LoginPage = () => {

  let navigate = useNavigate();

  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");

  const { setCurrentUser, setToken } = useContext(UserContext);

  const handleLogin = async (event) => {
    event.preventDefault()
    let data = {
      username: username,
      password: password
    }
    let post_data = qs.stringify(data);
    axios.post(`/api/login`, post_data,
      {
        headers: {
          'accept': 'application/json',
          'content-type': 'application/x-www-form-urlencoded'
        }
      })
      .then(response => {
        setCurrentUser(data.username);
        setToken(response.data.access_token)
        localStorage.setItem('token', response.data.access_token)
        localStorage.setItem('user', data.username)
        navigate('/task')
      })
      .catch(error => {
        console.error('There was an error!', error);
        alert("Cannot login, check your username/password")
      });
  }

  return (
    <div className='container'>
      <div className='title'><p>Login</p></div>
      <form onSubmit={handleLogin}>
        <FormControl isRequired>
          <div className='form'>
            <FormLabel>Username</FormLabel>
            <Input type='text' value={username} style={{ background: "white", border: "2px solid #E2E8F0" }} onChange={(e) => setUserName(e.target.value)} />
            <FormErrorMessage />
          </div>
          <div className='form'>
            <FormLabel>Password</FormLabel>
            <Input type='password' value={password} style={{ background: "white", border: "2px solid #E2E8F0" }} onChange={(e) => setPassword(e.target.value)} />
            <FormErrorMessage />
          </div>
          <div className='button'>
            <Button colorScheme='teal' size='lg' type="submit">
              Login
            </Button>
          </div>
          <p>Don't have an account?</p>
          <a href="/register">SIGN UP</a>
        </FormControl>
      </form>
    </div>
  );
}

export default LoginPage