import  React, { useState, useContext, useEffect } from 'react';
import { useParams, useNavigate } from "react-router-dom";
import {
    FormControl,
    FormLabel,
    FormErrorMessage,
    Input,
    Button,
    FormHelperText,
  } from '@chakra-ui/react'
import "./register.css"

import axios from "axios";
import qs from 'qs';

function RegisterPage() {
    let navigate = useNavigate();

    const [email, setEmail] = useState("");
    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");

    const handleRegister = async (e) => {
        e.preventDefault();
        let data = {
            "email": email,
            "username": username,
            "password": password,
        }
        console.log(data)
        let formData = new FormData();
        formData.append("user", JSON.stringify(data))
        console.log(formData)
        axios.post(`/api/register`, formData)
        .then(response => navigate((`/login`)))
        .catch(error => {
            console.log(error);
        });
    }

  return (
        <div className='container'>
            <div className='title'><p>Register</p></div>
            <form onSubmit={handleRegister}>
            <FormControl isRequired >
                <div className='form'>
                    <FormLabel>Email address</FormLabel>
                    <Input type='email' style={{background : "white" , border: "2px solid #E2E8F0"}} onChange={(e) => setEmail(e.target.value)}/>
                    <FormErrorMessage  />
                </div>
                <div className='form'>
                    <FormLabel>Username</FormLabel>
                    <Input type='text' style={{background : "white" , border: "2px solid #E2E8F0"}} onChange={(e) => setUserName(e.target.value)}/>
                    <FormErrorMessage  />
                </div>
                <div className='form'>
                    <FormLabel>Password</FormLabel>
                    <Input type='password' style={{background : "white" , border: "2px solid #E2E8F0"}} onChange={(e) => setPassword(e.target.value)}/>
                    <FormErrorMessage  />
                </div>
                <div className='button'>
                    <Button colorScheme='teal' size='lg' type="submit">
                    Register
                    </Button>
                </div>
                <p>Already have account?</p>
                <a href="/login">LOGIN</a>
            </FormControl>
            </form>
        </div>
      );
  }

export default RegisterPage