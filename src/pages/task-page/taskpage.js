import  React, { useState, useContext, useEffect } from 'react';
import { useParams, useNavigate } from "react-router-dom";
import {  VStack, 
          Avatar, 
          AvatarBadge, 
          AvatarGroup, 
          Button, 
          Flex,
          Box,
          Container,
          Alert,
          AlertIcon,
          Text,
          HStack,
          IconButton,
          Spacer,
          FormControl,
          FormLabel, 
          Input, 
          Modal, 
          ModalBody, 
          ModalCloseButton, 
          ModalContent, 
          ModalFooter, 
          ModalHeader, 
          ModalOverlay, 
          useDisclosure} from '@chakra-ui/react'
import axios from 'axios';
import { CheckIcon, AddIcon } from '@chakra-ui/icons'
import { UserContext } from '../auth/user-context';



function TaskPage() {
  const {token, getFromLocalStorage, currentUser} = useContext(UserContext);
  const { isOpen, onOpen, onClose } = useDisclosure() 
  const [title, setTitle] = useState("")
  const [description, setDescription] = useState("")
  const [deadline, setDeadline] = useState("")
  const [allTask, setAllTask] = useState([])
  const [taskName, setTaskName] = useState("")
  const [taskDesc, setTaskDesc] = useState("")
  const [taskDeadline, setTaskDeadline] = useState("")
  const [taskID, setTaskID] = useState("")
  const initialRef = React.useRef(null)
  const finalRef = React.useRef(null)
  let navigate = useNavigate();

  const ToDoBox = ({ task }) => {
    const { isOpen: isCheckOpen , onOpen: onCheckOpen, onClose: onCheckClose } = useDisclosure()
    if(task.creator == currentUser && task.status == 'todo'){
    return (
      <>
      <Box 
              minW='300px'
              maxW='300px'
              pt={4} 
              pl={2}
              pr={4}
              pb = {1}
              mt = {12} 
              mx = {16} 
              borderRadius='lg' 
              boxShadow='md' 
              textAlign='left'
              >
                <Text as='b'>{task.name}</Text>
                <Text noOfLines={[1, 2, 3, 4]}>{task.description}</Text>
                <Flex>
                  <Text fontSize='sm' mt={2}>
                    Due on {task.deadline.slice(11,16)} {task.deadline.slice(8,10)}/{task.deadline.slice(5,7)}/{task.deadline.slice(0,4)}
                  </Text>
                  <Spacer />
                  <IconButton 
                  aria-label='Done' 
                  backgroundColor={'white'}
                  color='teal'
                  icon={<CheckIcon />}
                  onClick = {onCheckOpen}/>
                </Flex>
                
      </Box>

      <Modal isOpen={isCheckOpen} onClose={onCheckClose}>
        <ModalOverlay />
          <ModalContent>
              <ModalHeader>Confirm</ModalHeader>
                  <ModalCloseButton />
                    <ModalBody>
                      Are you sure you're done with {task.name} task?
                    </ModalBody>

                    <ModalFooter>
                      <Button colorScheme='red' mr={3} onClick={onCheckClose}>
                        Close
                      </Button>
                      <Button onClick={(e)=> handleUpdate(e, task)} colorScheme='blue'>Confirm</Button>
                </ModalFooter>
          </ModalContent>
        </Modal>
        </>
    );
  }
  };

  const getCurrentTask = (task) =>{
    setTaskName(task.name);
    setTaskDeadline(task.deadline);
    setTaskDesc(task.description);
    setTaskID(task.id);
  }

  const getTask = () =>{
    axios.get('/api/task', 
    {
      headers: {
          "Authorization": `Bearer ${token}`
      }
    })
        .then((response)=> 
        { 
        var allTask = response.data;
        setAllTask(allTask);
        })
  }

  const UpdateData = (event) => {
    let data = {
      "name": taskName,
      "description": taskDesc,
      "deadline": taskDeadline,
      "status": "done" 
  }
    let formData = new FormData();
    formData.append("item", JSON.stringify(data))
    axios.put(`/api/task/${taskID}`, formData, {
        headers: {
            "Authorization": `Bearer ${token}`
        }
      })
     .then(response => {
        alert("Task Updated!")
        window.location.reload();
    })
     .catch(error => {
         console.error('There was an error!', error);
         error.response ? alert(error.response.data.detail):
         alert("Cannot update task")
         });
  }

  const handleUpdate = (e, task) => {
    getCurrentTask(task);
    UpdateData(e);
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    let data = {
        "name": title,
        "description": description,
        "deadline": deadline 
    }
    console.log(data)
    let formData = new FormData();
    formData.append("item", JSON.stringify(data))
    console.log(formData)
    console.log(token)
    axios.post('/api/task', formData, {
        headers: {
            "Authorization": `Bearer ${token}`
        }
      })
     .then(response => {
        window.location.reload();
    })
     .catch(error => {
         console.error('There was an error!', error);
         error.response ? alert(error.response.data.detail):
         alert("Cannot create task")
         });
    setDeadline("")
    setDescription("")
    setTitle("")
    }

    useEffect(() => {
      getFromLocalStorage()
    }, []);

    useEffect(() => {
      if(token){
        getTask()
      }
    }, [token]);



  return (
    <div className='taskpage'>
      <Flex p={3} direction='row-reverse' >
        <Avatar ml = {2} mt = {1} bg='teal.500' />
        <Button mt = {2} pr = {8} pl = {8} colorScheme='teal' variant='outline'>
          {currentUser}
        </Button>
        <IconButton 
        icon={<AddIcon />} 
        onClick={onOpen}
        mr = {2}
        mt = {2}
        />
        <Modal
          initialFocusRef={initialRef}
          finalFocusRef={finalRef}
          isOpen={isOpen}
          onClose={onClose}
        >
          <ModalOverlay />
          <ModalContent>
          <form onSubmit={handleSubmit}>
            <ModalHeader>Create a task</ModalHeader>
            <ModalCloseButton />
            <ModalBody pb={6}>
            
              <FormControl isRequired>
                <FormLabel>Title</FormLabel>
                <Input value={title} ref={initialRef} placeholder='ex: Assignments' onChange={(e) => setTitle(e.target.value)}/>
              </FormControl>
  
              <FormControl mt={4} isRequired>
                <FormLabel>Description</FormLabel>
                <Input value={description} placeholder='write something' onChange={(e) => setDescription(e.target.value)}/>
              </FormControl>
              
              <FormControl mt={4} isRequired>
                <FormLabel>Deadline</FormLabel>
                <Input
                    placeholder="Select Date and Time"
                    size="md"
                    type="datetime-local"
                    value={deadline}
                    onChange={(e) => setDeadline(e.target.value)}
                    />
              </FormControl>
            </ModalBody>
  
            <ModalFooter>
              <Button onClick={onClose} type="submit" colorScheme='blue' mr={3}>
                Save
              </Button>
              <Button onClick={onClose}>Cancel</Button>
            </ModalFooter>
            </form>
          </ModalContent>
        </Modal>
      </Flex>

      <Container mt={6} mb={6} maxW='4xl' centerContent>
      <Flex>
        <VStack>
            <Alert
            minW='300px'
            maxW='300px'
            py={4} 
            pl={4}
            pr={16} 
            mt = {12} 
            mb = {4}
            mx = {16} 
            status='warning' 
            borderRadius='lg' 
            boxShadow='md'>
            <AlertIcon />
              To Do List
            </Alert>
            {allTask.map((a) => (
              <ToDoBox task = {a}></ToDoBox>
            ))}
        </VStack>
        <VStack>
            <Alert 
            minW='300px'
            maxW='300px'
            py={4} 
            pl={4}
            pr={16} 
            mt = {12} 
            mb = {4} 
            mx = {16} 
            status='success' 
            borderRadius='lg' 
            boxShadow='md'>
            <AlertIcon />
              Done
            </Alert>  
            {allTask.map((a) => ( 
            a.creator == currentUser && a.status != 'todo' ? (
              <Box 
              minW='300px'
              maxW='300px'
              pt={2} 
              pl={2}
              pr={16}
              pb = {3}
              mt = {12} 
              mx = {16} 
              borderRadius='lg' 
              boxShadow='md' 
              textAlign='left'
              >
                <Text as='b'>{a.name}</Text>
                <Text noOfLines={[1, 2, 3, 4]}>{a.description}</Text>
                <Text fontSize='sm' mt={2}>Finished</Text>
              </Box>
            )
            : null
            ))}
        </VStack>
        </Flex>
      </Container>
    </div>
      );
  }

export default TaskPage